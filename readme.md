# Jeep Cherokee XJ
## Aftermarket IT/OT Software

## Information Technology Stack
* IDD (Integrated Dash Display)
* CID (Central Infotainment Display)
* BCM (Body Control Module)
* LCM (Lighting Control Module)
* PSS (Physical Security System)

## Operational Technology
* Aftermarket Engine Sensors
* Navigational Sensors (GNSS, IRS)
* SCM (Signal Control Module)
* Electrical Bus Management

### Integrated Dash Display
This component consists of a 12.6" 1920x515 LCD (non-touch-enabled). It is fitted to the OEM instrument panel in place of the six main analog gauges and LED indicators for high-beams and turn signals.
The stock instruments include the following:

* Engine Speed (Tachometer)
* Vehicle Speed (MPH/KPH)
* Water Temperature
* Fuel quantity
* Engine oil pressure
* Battery voltage

Additionally, the following should be added:

* Transmission oil temperature
* Fuel economy/burn rate

Operating System Options

| OS              | Base OS       | Cost  | Active | Last Release | Project Link |
| --------------- | ------------- | ----- | ------ | ------------ | ------------ |
| Raspberry Pi OS | Debian Linux  | Free  | Yes    |              |              |
| Cranshaft       | Unknown       | Free  | Semi   | 2022-06-29   | [Crankshaft](https://getcrankshaft.com/) |
| OpenAuto Pro    |               | $29   | Yes    |              | [OpenAuto Pro](https://bluewavestudio.io/shop/openauto-pro-car-head-unit-solution/) |


### Central Infotainment Display
This is the centrally mounted display for controlling HVAC, Music/Sound, and Navigation, as well as adjusting vehicle settings. I need to source an appropriate display, but it should be vertically oriented, and touch-enabled. It will probably be around 10" diagonal, and ideally 4:3 or 5:4 ratio to best match the stock cutouts. This will require relocating the center air vent, which could probably be where the stock ash tray sits.

If relocating the lower air vent is not feasible, then a horizontally oriented display of around 8.4" should suffice.

In either case, modification of the stock HVAC system is necessary to control the blower fan speed, and control of recirculation, heat, and upper/lower vent flaps.

### Body Control Module
This module is responsible for locking and unlocking of doors, opening and closing of electric windows, and providing the status of these to the CANBUS

### Lighting Control Module
This module is responsible for the manual and automatic switching of interior and exterior lighting. IP, ceiling, floor, head lamps, fog lamps, driving lamps, and flood

### Physical Security System
This module monitors door switches, interior RADAR/LIDAR, and IRS inputs and performs driver authentication checks. If doors are opened, cabin movement, or body movement is detected when the system is in a locked state, a siren/horn, and flashing of signal lights will be triggered