# LCM (Lighting Control Module)

The lighting control module is responsible for all exterior lighting except for legally-mandated indicators.

## LCM Inputs
* Headlamp Switch (Off-Auto-On)
* Ambient Light Sensor
* Rain Sensor


## LCM Outputs
* Low Beam
* High Beam
* Fog Lamps
* Driving Lamps
* Rear Flood Lamps
* Parking Lamps