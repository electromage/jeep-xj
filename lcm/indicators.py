import board
import digitalio
import adafruit_adxl34x

# Define input pins
input_left_turn = digitalio.DigitalInOut(board.D0)
input_left_turn.direction = digitalio.Direction.INPUT

input_right_turn = digitalio.DigitalInOut(board.D1)
input_right_turn.direction = digitalio.Direction.INPUT

input_brake_switch = digitalio.DigitalInOut(board.D3)
input_brake_switch.direction = digitalio.Direction.INPUT

# Define output pins
output_left_turn = digitalio.DigitalInOut(board.D4)
output_left_turn.direction = digitalio.Direction.OUTPUT

output_right_turn = digitalio.DigitalInOut(board.D5)
output_right_turn.direction = digitalio.Direction.OUTPUT

output_brake = digitalio.DigitalInOut(board.D6)
output_brake.direction = digitalio.Direction.OUTPUT

# Initialize I2C bus
i2c = busio.I2C(board.SCL, board.SDA)

# Initialize ADXL345 accelerometer
accelerometer = adafruit_adxl34x.ADXL345(i2c)

# Initialize filter variables
alpha = 0.5
filtered_acceleration = [0, 0, 0]

# Read input pins and control output pins
while True:
    if input_left_turn.value:
        output_left_turn.value = True
    else:
        output_left_turn.value = False

    if input_right_turn.value:
        output_right_turn.value = True
    else:
        output_right_turn.value = False
    
    # Read acceleration data from accelerometer and apply low-pass filter
    acceleration = accelerometer.acceleration
    filtered_acceleration[0] = alpha * filtered_acceleration[0] + (1 - alpha) * acceleration[0]
    filtered_acceleration[1] = alpha * filtered_acceleration[1] + (1 - alpha) * acceleration[1]
    filtered_acceleration[2] = alpha * filtered_acceleration[2] + (1 - alpha) * acceleration[2]

    # Set output pin D4 high if filtered acceleration is below -0.25
    if filtered_acceleration[0] < -0.25:
        output_brake.value = True
    else:
        output_brake.value = False

    # Print filtered acceleration data
    print("Filtered Acceleration: X = %.2fG, Y = %.2fG, Z = %.2fG" % tuple(filtered_acceleration))