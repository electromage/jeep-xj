import board
import busio
import adafruit_bh1750
import neopixel
import time

i2c = busio.I2C(board.SCL, board.SDA)
light_sensor = adafruit_bh1750.BH1750(i2c)
pixel_pin = board.D5
num_pixels = 1
pixel = neopixel.NeoPixel(board.NEOPIXEL, 1)

threshold = 80
avg_intensity = 0
# Set the time interval between each reading
reading_interval = 0.2

def get_average_intensity(num_readings):
    # Take multiple readings and calculate the average light intensity
    total_intensity = 0
    count = 0
    for i in range(num_readings):
        intensity = light_sensor.lux
        if intensity > 80:
            intensity = 80
        total_intensity += intensity
        count += 1
        time.sleep(reading_interval)
    if count > 0:
        avg_intensity = total_intensity / count
    else:
        avg_intensity = 0
    return avg_intensity

while True:
    # Check if the average light intensity is below the threshold
    if avg_intensity < threshold:
        pixel.fill((0, 127, 0))
        avg_intensity = get_average_intensity(25)
    else:
        pixel.fill((127, 0, 0))
        avg_intensity = get_average_intensity(20)
