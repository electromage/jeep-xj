# LCM Program (Write directly to flash)

import board
import busio
import adafruit_bh1750
import neopixel
import time
import digitalio
import can

# Define pin numbers for digital inputs and outputs
auto_pin = board.D6
on_pin = board.D7
headlamps_pin = board.D8
parkinglamps_pin = board.D9

# Initialize digital inputs
auto_input = digitalio.DigitalInOut(auto_pin)
auto_input.direction = digitalio.Direction.INPUT
auto_input.pull = digitalio.Pull.DOWN

on_input = digitalio.DigitalInOut(on_pin)
on_input.direction = digitalio.Direction.INPUT
on_input.pull = digitalio.Pull.DOWN

# Initialize digital outputs
headlamps_output = digitalio.DigitalInOut(headlamps_pin)
headlamps_output.direction = digitalio.Direction.OUTPUT

parkinglamps_output = digitalio.DigitalInOut(parkinglamps_pin)
parkinglamps_output.direction = digitalio.Direction.OUTPUT

i2c = busio.I2C(board.SCL, board.SDA)
light_sensor = adafruit_bh1750.BH1750(i2c)
pixel_pin = board.D5
num_pixels = 1
pixel = neopixel.NeoPixel(board.NEOPIXEL, 1)

threshold = 80
avg_intensity = 0
reading_interval = 0.2

# Initialize CANbus
can_bus = can.Bus()  # Initialize CANbus using the appropriate configuration

# Define CANbus message IDs for inputs and outputs
input_msg_id = 0x100
output_msg_id = 0x200

# Define input and output message structures
input_msg = can.Message()
output_msg = can.Message()

def get_average_intensity(num_readings):
    total_intensity = 0
    count = 0
    for i in range(num_readings):
        intensity = light_sensor.lux
        if intensity > 80:
            intensity = 80
        total_intensity += intensity
        count += 1
        time.sleep(reading_interval)
    if count > 0:
        avg_intensity = total_intensity / count
    else:
        avg_intensity = 0
    return avg_intensity

while True:
    # Read the state of digital inputs
    auto_state = auto_input.value
    on_state = on_input.value

    # Read CANbus messages
    received_msgs = can_bus.receive()  # Adjust according to your CANbus library's receive function

    # Process received CANbus messages
    for msg in received_msgs:
        if msg.arbitration_id == input_msg_id:
            # Process input message and update the state accordingly
            # ...

    # Check if both digital inputs are low (off)
    if auto_state == False and on_state == False:
        headlamps_output.value = False
        parkinglamps_output.value = False
        pixel.fill((0, 0, 0))
        avg_intensity = get_average_intensity(25)
    
    # Check if auto input is high (parkinglamps on)
    elif auto_state == True:
        parkinglamps_output.value = True
        avg_intensity = get_average_intensity(25)
        if avg_intensity < threshold:
            headlamps_output.value = True
            pixel.fill((0, 127, 0))
            # Send output status over CANbus
            output_msg.arbitration_id = output_msg_id
            output_msg.data = [1, 1]  # Adjust according to your CANbus message structure
            can_bus.send(output_msg)  # Adjust according to your CANbus library's send function
        else:
            headlamps_output.value = False
            pixel.fill((127, 0, 0))
            # Send output status over CANbus
            output_msg.arbitration_id = output_msg_id
            output_msg.data = [0, 1]  # Adjust according to your CANbus message structure
            can_bus.send(output_msg)  # Adjust according to your CANbus library's send function
    
    # Check if on input is high (all outputs on)
    elif on_state == True:
        headlamps_output.value = True
        parkinglamps_output.value = True
        pixel.fill((127, 127, 127))
        avg_intensity = get_average_intensity(20)
        # Send output status over CANbus
        output_msg.arbitration_id = output_msg_id
        output_msg.data = [1, 1]  # Adjust according to your CANbus message structure
        can_bus.send(output_msg)  # Adjust according to your CANbus library's send function
